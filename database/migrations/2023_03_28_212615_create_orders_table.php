<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {


    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('table_id')->constrained()->noActionOnDelete();
            $table->foreignId('shift_user_id')->constrained('shift_user')->noActionOnDelete();
            $table->timestamp('created_at')->useCurrent();
            $table->enum('status', ['AC', 'PR', 'RE', 'PA', 'CA']);
            $table->unsignedTinyInteger('number_of_people');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};