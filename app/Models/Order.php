<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends AbstractModel
{
    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(ShiftUser::class)->user();
    }

    public function positions(): BelongsToMany
    {
        return $this->belongsToMany(Position::class)->using(OrderPosition::class);
    }

}