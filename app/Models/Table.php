<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Table extends AbstractModel
{
    use HasFactory;
}