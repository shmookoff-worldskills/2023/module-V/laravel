<?php

namespace App\Enums;

enum UserStatusEnum: int
{
    use EnumValues;
    case Free = 1;
    case Working = 2;
}