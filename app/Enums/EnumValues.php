<?php

namespace App\Enums;

trait EnumValues
{
    public static function getValues(): array
    {
        $values = [];
        foreach (self::cases() as $enum) {
            $values[] = $enum->value;
        }
        return $values;
    }
}