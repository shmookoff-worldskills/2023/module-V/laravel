<?php

namespace App\Enums;

enum UserGroupEnum: int
{
    use EnumValues;
    case Administrator = 1;
    case Server = 2;
    case Cook = 3;
}