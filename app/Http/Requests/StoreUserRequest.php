<?php

namespace App\Http\Requests;

use App\Enums\UserGroupEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'surname' => ['string'],
            'patronymic' => ['string'],
            'login' => ['required', 'string', 'unique:users'],
            'password' => ['required', 'string'],
            'photo_file' => [Rule::imageFile()],
            'role_id' => ['required', Rule::in(UserGroupEnum::getValues())]
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'group' => (int) $this->role_id,
        ]);
    }
}