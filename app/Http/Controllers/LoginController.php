<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(LoginRequest $request)
    {
        $user = User::where('login', $request->login)
            ->where('password', $request->password)
            ->firstOr(function () {
                abort(Response::HTTP_UNAUTHORIZED, 'Login failed');
            });

        $token = $user->createToken('api-token');

        return ['token' => $token->plainTextToken];
    }
}
