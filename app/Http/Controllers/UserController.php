<?php

namespace App\Http\Controllers;

use App\Enums\UserGroupEnum;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\CreatedUserResource;
use App\Http\Resources\UserCollection;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new UserCollection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $avatar = $request->file('photo_file');
        if (!empty($avatar)) {
            $avatar->storePubliclyAs('photos', $request->login . '.' . $avatar->extension());
        }

        var_dump($request->all());

        $user = new User;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->patronymic = $request->patronymic;
        $user->login = $request->login;
        $user->password = $request->password;
        $user->group = $request->group;
        $user->save();

        return new CreatedUserResource($user);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}