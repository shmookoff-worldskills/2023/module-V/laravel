<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreShiftUserRequest;
use App\Http\Requests\UpdateShiftUserRequest;
use App\Models\ShiftUser;

class ShiftUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreShiftUserRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ShiftUser $shiftUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ShiftUser $shiftUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateShiftUserRequest $request, ShiftUser $shiftUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ShiftUser $shiftUser)
    {
        //
    }
}
